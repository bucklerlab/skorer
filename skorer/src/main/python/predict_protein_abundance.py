import pandas as pd
import numpy as np
import random
from Bio.SeqIO import parse
from keras.utils import np_utils
from keras.layers import *
from keras.optimizers import Adam
from keras.models import Sequential,Model
from keras.callbacks import EarlyStopping
import os

# functions
LEN_TAR=500
NTS=dict([["A",[1,0,0,0]],["C",[0,1,0,0]],["G",[0,0,1,0]],["T",[0,0,0,1]],["N",[0,0,0,0]]]) 
def seq2num(seq,LEN_TAR,align): # align='left' or 'right'
   if align=='left':
      if len(seq)<LEN_TAR:
         seq=seq+"N"*(LEN_TAR-len(seq))
      else:
         seq=seq[:LEN_TAR]
      nums=np.array([NTS[x] for x in seq])
      return nums
   if align=='right':
      if len(seq)<LEN_TAR:
         seq="N"*(LEN_TAR-len(seq))+seq
      else:
         seq=seq[-LEN_TAR:]
      nums=np.array([NTS[x] for x in seq])
      return nums

# load transcript seqs (from EnsemblPlant)
seqs=dict([(x.id,str(x.seq)) for x in parse('Zea_mays.AGPv3.31.cdna.all.fa','fasta')])

# load proteome data from https://www.pnas.org/content/110/49/E4808
protein_abundance=pd.read_csv('Proteome_AGPv3_Waley_PNAS.csv',index_col=1,sep='\t')
txids=protein_abundance.index.values
targets=np.array([np.max(protein_abundance.iloc[i,1:]) for i in range(protein_abundance.shape[0])])
predictors=[]
count=0
for txid in txids:
   if txid in seqs:
      predictor=np.concatenate([seq2num(seqs[txid],LEN_TAR,'left'),seq2num(seqs[txid],LEN_TAR,'right')])
      predictors.append(predictor)
   else:
      count+=1
      print str(count)+' '+txid+" not in EnsemblPlant cdna seqs"
      predictors.append('NO_SEQ')

predictors_final=np.array([x for x in predictors if x!='NO_SEQ'])
targets_final=np.array([math.log10(targets[i]+1) for i,x in enumerate(predictors) if x!='NO_SEQ'])

# splitting
# random splitting used here, will do family-aware splitting
datalen=len(predictors_final)

train_indices=random.sample(range(datalen),int(datalen*0.8))
test_indices=list(set(range(datalen))-set(train_indices))

predictors_train=predictors_final[train_indices]
predictors_test=predictors_final[test_indices]

targets_train=targets_final[train_indices]
targets_test=targets_final[test_indices]

# model training
def build():
   model=Sequential()

   model.add(Conv1D(128,kernel_size=6,padding='valid',activation='relu',input_shape=(LEN_TAR*2,4)))
   model.add(Conv1D(128,kernel_size=6,padding='valid',activation='relu'))
   model.add(MaxPooling1D(pool_size=6,padding='valid'))
   model.add(Dropout(0.1))

   model.add(Conv1D(128,kernel_size=6,padding='valid',activation='relu'))
   model.add(Conv1D(128,kernel_size=6,padding='valid',activation='relu'))
   model.add(MaxPooling1D(pool_size=6,padding='valid'))
   model.add(Dropout(0.1))

   model.add(Conv1D(64,kernel_size=6,padding='valid',activation='relu'))
   model.add(Conv1D(64,kernel_size=6,padding='valid',activation='relu'))
   model.add(MaxPooling1D(pool_size=3,padding='valid'))
   model.add(Dropout(0.1))

   model.add(Flatten())
   model.add(Dense(64,activation='relu'))
   model.add(Dropout(0.1))
   model.add(Dense(32,activation='relu'))
   model.add(Dense(1))

   return model

callbacks=[EarlyStopping(monitor='val_loss',patience=3,verbose=0,restore_best_weights=True)]

model=build()
model.compile(loss='mean_squared_error',optimizer='adam',metrics=['mse'])
model.summary()

model.fit(x = predictors_train,
          y = targets_train,
          validation_data = (predictors_test,targets_test),
          batch_size=512,
          epochs=100,
          shuffle=True,
          callbacks=callbacks)
model.s
prediction=model.predict(predictors_test)
R=np.corrcoef(np.squeeze(prediction),targets_test)[0,1]
print R

##################################################################################################
# score all scripts in maize V3 genome
all_txids=seqs.keys()
all_predictors=np.array([np.concatenate([seq2num(seqs[x],LEN_TAR,'left'),seq2num(seqs[x],LEN_TAR,'right')]) for x in all_txids])
all_prediction=np.squeeze(model.predict(all_predictors))
scores=pd.DataFrame(list(zip(all_txids,all_prediction)),columns=['txid','score'])
scores.to_csv('scores.csv')













