{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# predict_processing\n",
    "import os\n",
    "import sys\n",
    "import kipoi\n",
    "from subprocess import Popen, PIPE\n",
    "\n",
    "\n",
    "\n",
    "def run_model(model_name, gtf, fasta, length, output):\n",
    "    arg1 = '\"fasta_file\":{},'.format(fasta)\n",
    "    arg2 = ' \"gtf_file\":{},'.format(gtf)\n",
    "    arg3 = ' \"length\":{} '.format(length)\n",
    "    arg4 = ['kipoi', 'predict', model_name,\n",
    "            '--dataloader_args={' + arg1 + arg2 + arg3 + '}',\n",
    "            '-o', '{}'.format(output)]\n",
    "    print(arg4)\n",
    "    Popen(arg4)\n",
    "    return output"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
