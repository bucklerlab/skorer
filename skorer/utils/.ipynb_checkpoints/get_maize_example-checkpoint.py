{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#get maize example\n",
    "import os\n",
    "import sys\n",
    "import kipoi\n",
    "from subprocess import Popen, PIPE\n",
    "\n",
    "\n",
    "# gunzip, pick chr10, take away gramene, get final\n",
    "gtf_gz = 'ftp://ftp.gramene.org/pub/gramene/release-61/gtf/zea_mays/Zea_mays.B73_RefGen_v4.43.gtf.gz'\n",
    "fasta_gz = 'ftp://ftp.gramene.org/pub/gramene/release-61/fasta/zea_mays/dna/Zea_mays.B73_RefGen_v4.dna.toplevel.fa.gz'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def wget_maize(gtf_gz,fasta_gz):\n",
    "    \"\"\"get .gz files from MaizeGDB\"\"\"\n",
    "    arg1 = ['wget', gtf_gz]\n",
    "    arg2 = ['wget', fasta_gz]\n",
    "    gtf_gz = Popen(arg1)\n",
    "    fasta_gz = Popen(arg2)\n",
    "    return gtf_gz, fasta_gz\n",
    "\n",
    "def get_maize_example():\n",
    "    \"\"\"unzip .gz files to get gtf files\"\"\"\n",
    "    wget_maize(gtf_gz,fasta_gz)\n",
    "    arg1 = ['gunzip', gtf_gz]\n",
    "    arg2 = ['gunzip', fasta_gz]\n",
    "    maize_gtf = Popen(arg1)\n",
    "    maize_fatsa = Popen(arg2)\n",
    "    return maize_gtf, maize_fatsa\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
