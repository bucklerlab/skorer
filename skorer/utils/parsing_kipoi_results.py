import pandas as pd
import numpy as np
from collections import OrderedDict

def run_model(model_name, gtf, fasta, output, length=70):
    import kipoi
    import pysam
    from subprocess import Popen
    
    !kipoi env install {model_name}
    pysam.faidx(fasta) #to be sure that fasta file is indexed
    fasta_arg = '\"fasta_file\":{},'.format(fasta)
    gtf_arg = ' \"gtf_file\":{}'.format(gtf)
    if model_name == "labranchor":
        length_arg = ' \"length\":{} '.format(length)
        kipoi_cml = ['kipoi', 
                 'predict', 
                 model_name, 
                 '--dataloader_args={' + fasta_arg + gtf_arg + length_arg + '}', 
                 '-o', 
                 '{}'.format(output)]
    else:
        kipoi_cml = ['kipoi', 
                 'predict', 
                 model_name, 
                 '--dataloader_args={' + fasta_arg + gtf_arg +'}', 
                 '-o', 
                 '{}'.format(output)]
    print(kipoi_cml)
    Popen(kipoi_cml)

def parsing_labranchor(labranchor_df, transcript_summary_file):
    results_dict = OrderedDict()
    positionList = [int(preds.split("/")[1]) for preds in labranchor_df.columns.tolist()[12:82]]
    max_labranchor_preds = list()
    pos_labranchor_preds = list()
    for index,row in labranchor_df.iterrows():
        gene_id =row['metadata/geneID']
        gene_model_id =row['metadata/transcriptID']
        score = np.max(row.values.tolist()[12:82])
        branchingPoint = row.values.tolist()[6] + positionList[np.argmax(row.values.tolist()[12:82])]
        max_labranchor_preds.append(score)
        pos_labranchor_preds.append(branchingPoint)
        if gene_id in results_dict:
            if gene_model_id in results_dict[gene_id]:
                results_dict[gene_id][gene_model_id][branchingPoint] = score
            else:
                results_dict[gene_id][gene_model_id] = dict()
                results_dict[gene_id][gene_model_id][branchingPoint] = score
        else:
            results_dict[gene_id] = OrderedDict()
            results_dict[gene_id][gene_model_id] = OrderedDict()
            results_dict[gene_id][gene_model_id][branchingPoint] = score
    
    evaluation_gene_models = list()
    for gene_id in results_dict:
        for transcript in results_dict.get(gene_id):
            branching_median_score = np.median(list(results_dict.get(gene_id)[transcript].values()))
            branching_max_score = np.max(list(results_dict.get(gene_id)[transcript].values()))
            branching_min_score = np.min(list(results_dict.get(gene_id)[transcript].values()))
            evaluation_gene_models.append(OrderedDict({"gene_id":gene_id, 
                                                   "transcript_id":transcript,
                                                   "branching_median_score":branching_median_score,
                                                   "branching_min_score":branching_min_score,
                                                   "branching_max_score":branching_max_score}))
        
    labranchor_evaluation_report = pd.DataFrame(evaluation_gene_models)
    labranchor_evaluation_report.to_csv("../results/"+transcript_summary_file, 
                     header=True,
                     sep=",",
                     index=False)


def parsing_optimus_5prime(optimus_5prime_df, transcript_summary_file):    
    results_dict = OrderedDict()
    for index,row in optimus_5prime_df.iterrows():
        gene_id =row['metadata/gene_id']
        gene_model_id =row['metadata/transcript_id']
        score = row[['preds/0']]
        if gene_id in results_dict:
            results_dict[gene_id][gene_model_id] = score
        else:
            results_dict[gene_id] = OrderedDict()
            results_dict[gene_id][gene_model_id] = score
            
    preds = optimus_5prime_df['preds/0']
    optimus_5prime_df = optimus_5prime_df[['metadata/gene_id',
                                           'metadata/transcript_id',
                                           'preds/0']]
    optimus_5prime_df.to_csv("../results/"+transcript_summary_file, 
                     header=False,
                     sep=",",
                     index=False)  
                     
def parsing_maxentscan(maxentscan_df, transcript_summary_file):
    results_dict = OrderedDict()
    for index,row in maxentscan_df.iterrows():
        gene_id =row['metadata/geneID']
        gene_model_id =row['metadata/transcriptID']
        score = row['preds']
        region = '{}:{}'.format(row['metadata/ranges/start'], row['metadata/ranges/end'])
        if gene_id in results_dict:
            if gene_model_id in results_dict[gene_id]:
                results_dict[gene_id][gene_model_id][region] = score
            else:
                results_dict[gene_id][gene_model_id] = OrderedDict()
                results_dict[gene_id][gene_model_id][region] = score
        else:
            results_dict[gene_id] = OrderedDict()
            results_dict[gene_id][gene_model_id] = OrderedDict()
            results_dict[gene_id][gene_model_id][region] = score
            
    evaluation_gene_models = list()
    for gene_id in results_dict:
        for transcript in results_dict.get(gene_id):
            median_score = np.median(list(results_dict.get(gene_id)[transcript].values()))
            max_score = np.max(list(results_dict.get(gene_id)[transcript].values()))
            min_score = np.min(list(results_dict.get(gene_id)[transcript].values()))
            evaluation_gene_models.append(OrderedDict({"gene_id":gene_id, 
                                                   "transcript_id":transcript,
                                                   "median_score":median_score,
                                                   "min_score":min_score,
                                                   "max_score":max_score,}))
    
    evaluation_report = pd.DataFrame(evaluation_gene_models)
    evaluation_report.to_csv("../results/"+transcript_summary_file, 
                     header=True,
                     sep=",",
                     index=False)        
                      