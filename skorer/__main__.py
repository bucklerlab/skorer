"""A main script that contain many things so that you don't have to use input()"""
import os
import sys
import argparse
import kipoi
import helper
import reporter


def read_arguments(arguments=None):
    """The main routine in charge of parsing the user options and make the calls to the code
    Parameters
    ----------
    arguments
    """
    text = "A tool for atomatically running kipoi models."
    parser = argparse.ArgumentParser(prog="run_kipoi_model",
                                     description=text,
                                     usage="%(prog)s [options]")
    
     # two main commands run_model and get_example
    subparsers = parser.add_subparsers(help="run_model: run any of transcript features kipoi models",
                                       dest="which")

    run_model_parser = subparsers.add_parser("run_model",
                                                 description="usage: python skore run_model \n" +
                                                             "-model_name labranchor\n" +
                                                             "-fasta fasta_file example.fa\n" +
                                                             "-gtf gtf_file my_gtf.gtf\n" +
                                                             "-output my_output.tsv")
    run_model_parser.add_argument("-model_name",
                                      type=str,
                                      action="store",
                                      dest="model_name",
                                      help= "Input kipoi model name, include: Optimus_5Prime,labranchor,MaxEntScan/3prime,MaxEntScan/5prime",
                                      required=True)
    run_model_parser.add_argument("-fasta",
                                      action="store",
                                      dest="fasta_file",
                                      help= "input fasta and fai files",
                                      required=True)
    run_model_parser.add_argument("-gtf",
                                      action="store",
                                      dest="gtf_file",
                                      help= "input gtf files",
                                      required=True)
    run_model_parser.add_argument("-output",
                                      type=str
                                      action="store",
                                      dest="output_file",
                                      help= "prediction tsv files",
                                      required=True)
    
    subparsers = parser.add_subparsers(help="get_example: run models with examples for maize chr10 and human chr22",
                                       dest="which")
    get_example_parser = subparsers.add_parser("get_example",
                                                 description="usage: python skore get_example -model_name \n "+
                                                              "-model_name labranchor")
    get_example_parser.add_argument("-model_name",
                                      type=str,
                                      action="store",
                                      dest="model_name",
                                      help= "Input kipoi model name, include: Optimus_5Prime,labranchor,MaxEntScan/3prime,MaxEntScan/5prime",
                                      required=True)
    arguments = parser.parse_args()
    return arguments


if __name__ == "__main__":
    args = utils.read_arguments()
    run_id = utils.get_run_id()
    if args.which == "run_model":
        model_name= args.model_name
        fasta = args.fasta
        gtf = args.gtf
        output = args.output
        logger = utils.get_logger(args, run_id)
    elif args.which == 'get_example':
        run_id = get_run_id()
        logger = utils.get_logger(args, run_id)
<<<<<<< HEAD
=======

>>>>>>> 7076bd99b6b63c1d174a11d78b92e3b9f60c4933
