# README #
This repository host code and examples notebooks to  
use several machine learning models in a kipoi repository  
to evaluate gene models hypothesis

### Evaluation of features ###

### Summary ###
Different pipelines are suited for the annotation of eukaryotic gene models using several lines of evidences.   
We have put together a pipeline with [models](https://kipoi.org/) trained in functional genomic data specific for some features that can help to score transcripts for each gene model, and that are not part (i.e., the models and data) of the pipelines for gene model annotation.  

Currently skorer can evaluated the following features:  
Sequences in the 3' and 5' exon-introns junctions [MaxEntScan5'](http://kipoi.org/models/MaxEntScan/5prime/) and [MaxEntScan3'](http://kipoi.org/models/MaxEntScan/3prime/)  
Sequence in the expected positon for the branching point [LaBranchoR](http://kipoi.org/models/labranchor/)  
Sequence in the 5'UTR region flanking the translation start site [Optimus_5Prime](http://kipoi.org/models/Optimus_5Prime/)  
Using transcript sequences to predict protein abundances (Hai Wang)

The two inputs for predicting protein abundances are: (1) SpreadSheet for Protein abundance (https://docs.google.com/spreadsheets/d/1pPKVHzAveanuVtdFh5jzUT6jMTnK_pQ3ExGL2uYcq0M/edit#gid=0). Data was published on PNAS (https://www.pnas.org/content/110/49/E4808). (2) mRNA sequences for maize (ftp://ftp.ensemblgenomes.org/pub/plants/release-31/fasta/zea_mays/cdna/Zea_mays.AGPv3.31.cdna.all.fa.gz).
For other models, the two inputs to run skorer are a GFF file (with gene models and transcripts), and a fasta file for the corresponding genome

![](skorer/docs/png_files/Figure1_cartoon.png)

### Who do I talk to? ###

* Jesse (Sijie) Zhou (sz546@cornell.edu)
* Maria Katherine Mejia-Guerra (mm2842@cornell.edu)
* Hai Wang (hw449@cornell.edu)
* Ed Buckler