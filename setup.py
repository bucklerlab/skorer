#!/usr/bin/env python3

from setuptools import setup, find_packages


DESCRIPTION = "A pipeline that combines machine learning models" \ 
              "to evaluate annotated transcripts from DNA sequences"

def readme():
    with open('README.rst') as f:
        return f.read()


setup(name="kmergrammar",
      version="0.0.1",
      author="Katherine Mejia-Guerra",
      author_email="mm2842@cornell.edu",
      description="Several machine/deep learning models to inspect DNA sequences to evaluate annotated transcripts",
      long_description=readme(),
      long_description_content_type="text/markdown",
      download_url="https://bitbucket.org/bucklerlab/skorer/src/master/",
      url="https://bitbucket.org/bucklerlab/skorer/src/master/",
      packages=find_packages(),
      entry_points={'console_scripts': ['skorer = skorer.__main__:main']},
      classifiers=[
          "Natural Language :: English",
          "Topic :: Scientific/Engineering :: Bio-Informatics",
          "Intended Audience :: Science/Research",
          "Development Status :: 3 - Alpha",
          "Programming Language :: Python :: 3",
          "License :: OSI Approved :: MIT License",
          "Operating System :: MacOS :: MacOS X",
          "Operating System :: Linux :: Ubuntu"],
      license='GPLv3',
      install_requires=['markdown', 'numpy', 'pandas', 'pysam','pybigwig','kipoi', 'gensim', 'scikit-image', 'scikit-learn', 'seaborn','matplotlib',
                        'pyfaidx', 'pybedtools'],
      test_suite='nose.collector',
      tests_require=['nose'],
      scripts=['bin/'],
      include_package_data=True,
      zip_safe=False)
